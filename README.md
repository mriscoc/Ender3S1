# Professional Firmware for the Creality Ender 3 s1 3D Printer 

![GitHub](https://img.shields.io/github/license/mriscoc/Ender3S1.svg)
![GitHub contributors](https://img.shields.io/github/contributors/mriscoc/Ender3S1.svg)
![GitHub Release Date](https://img.shields.io/github/release-date/mriscoc/Ender3S1.svg)
[![Build Status](https://github.com/mriscoc/Ender3S1/workflows/CI/badge.svg?branch=bugfix-2.0.x)](https://github.com/mriscoc/Ender3S1/actions)

<img align="right" width=200 src="buildroot/share/pixmaps/Ender3s1.jpg" />

Please test this firmware and let us know if it misbehaves in any way. Volunteers are standing by!

## Download a compilated version of this firmware

Released versions of this firmware can be found on the [Releases page](https://github.com/mriscoc/Ender3S1/releases).

![](https://raw.githubusercontent.com/mriscoc/Marlin_Ender3v2/Ender3v2-Released/screenshots/main.jpg)

## Wiki
 - [How to install the firmware](https://github.com/mriscoc/Ender3S1/wiki/How-to-install-the-firmware)
 - [Installing a 3D/BLTouch](https://github.com/mriscoc/Ender3S1/wiki/3D-BLTouch)
 - [Color themes](https://github.com/mriscoc/Ender3S1/wiki/Color-Themes)
 - [How to use with Octoprint](https://github.com/mriscoc/Ender3S1/wiki/Octoprint)
  
## Community links
* [Telegram]
* [Reddit](https://www.reddit.com/r/Ender3v2Firmware) 
* [Facebook](https://www.facebook.com/groups/ender3s1printer)

## Credits

This firmware is maintained by Miguel Risco-Castillo [[@mriscoc](https://github.com/mriscoc)] - Peru  

This work would not be possible without the supporters, helpers and betatesters at the Telegram group.

## Disclaimer

THIS FIRMWARE AND ALL OTHER FILES IN THE DOWNLOAD ARE PROVIDED FREE OF CHARGE WITH NO WARRANTY OR GUARANTEE. SUPPORT IS NOT INCLUDED JUST BECAUSE YOU DOWNLOADED THE FIRMWARE.

WE ARE NOT LIABLE FOR ANY DAMAGE TO YOUR PRINTER, PERSON, OR ANY OTHER PROPERTY DUE TO USE OF THIS FIRMWARE. IF YOU DO NOT AGREE TO THESE TERMS THEN DO NOT USE THE FIRMWARE.

